package by.bsuir.writer.model;

/**
 * Created by anton on 17/10/15.
 */
public interface ILyric {
    String getText();
    void setText(String text);
}
