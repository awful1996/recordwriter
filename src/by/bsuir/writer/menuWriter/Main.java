package by.bsuir.writer.menuWriter;


import by.bsuir.writer.DemoLog;
import by.bsuir.writer.collections.Album;
import by.bsuir.writer.logic.ActionWithAlbum;
import by.bsuir.writer.model.Record;
import by.bsuir.writer.model.Time;
import by.bsuir.writer.parser.Xml;
import by.bsuir.writer.rom.Cd;
import by.bsuir.writer.rom.Dvd;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by anton on 13/10/15.
 */
public class Main {

    private static Logger logger = Logger.getLogger(DemoLog.class);

    public static void main(String[] args) {

        Album album = new Album();
        album.setArrayOfRecord(new Xml().openXmlFile("/home/anton/SongWriter/data.xml"));
        ActionWithAlbum.printNameOfSong(album.getArrayOfRecord());
        System.out.println(ActionWithAlbum.getCommonTime(album.getArrayOfRecord()));
        try {
            ActionWithAlbum.findRecord(new Time(0, 5, 0), new Time(0, 10, 0), album.getArrayOfRecord());
        } catch (Exception e) {
            logger.error("error", e);
        }
        Cd writer = new Cd();
        writer.burn(album);
        Dvd writerDvd = new Dvd();
        writerDvd.burn(album);
        try {
            for(Record item :ActionWithAlbum.sortByGenre(album.clone())){
                System.out.println(item.getName());
            }

        }
        catch(CloneNotSupportedException e){

        }
        for(Record item :album.getArrayOfRecord()){
            System.out.println(item.getName());
        }

    }
}