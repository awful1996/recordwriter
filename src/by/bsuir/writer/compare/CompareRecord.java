package by.bsuir.writer.compare;

import by.bsuir.writer.model.Record;

import java.util.Comparator;

/**
 * Created by anton on 18/10/15.
 */
public class CompareRecord implements Comparator<Record> {
    @Override
    public int compare(Record obj1, Record obj2) {
        return obj1.getGenre().compareTo(obj2.getGenre());
    }
}
