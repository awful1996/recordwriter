package by.bsuir.writer.model;

/**
 * Created by anton on 13/10/15.
 */
public class Time implements Comparable<Time> {

    private int hours;
    private int min;
    private int sec;

    public Time(){
        this.hours = 0;
        this.min = 0;
        this.sec = 0;
    }

    public Time (int hours, int min, int sec)throws Exception {
        this.hours = hours;

        if (min < 60) {
            this.min = min;
        } else {
            new Exception("be sec < 60");
        }
        if (sec < 60) {
            this.sec = sec;
        } else {
            new Exception("be sec < 60");
        }
    }


    public void setHours(int hours) {
        this.hours = hours;
    }


    public int getHours() {
        return hours;
    }

    public void setSec(int sec) throws Exception{
        if (sec < 60) {
            this.sec = sec;
        }
        else{
            throw new Exception("be sec < 60");
        }
    }

    public void setMin(int min) throws Exception {
        if (min < 60) {
            this.min = min;
        }
        else{
            throw new Exception("be sec < 60");
        }
    }

    public int getSec() {

        return sec;
    }

    public int getMin() {

        return min;
    }

    @Override
    public int hashCode() {
        int result = 3600 * hours + 60*min + sec;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Time other = (Time) obj;
        if (hours != other.getHours())
            return false;
        if (min != other.getMin())
            return false;
        if (sec != other.getSec())
            return false;
        return true;
    }

    @Override
    public int compareTo(Time obj) {

        int timeInSec = hours * 3600 + min * 60 + sec;
        int objInSec = obj.getHours() * 3600 + obj.getMin() * 60 + obj.getSec();

        return objInSec - timeInSec;
    }

    @Override
    public String toString() {
        return hours + ":" + min + ":" + sec;
    }
}
