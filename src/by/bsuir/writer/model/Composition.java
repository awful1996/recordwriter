package by.bsuir.writer.model;

/**
 * Created by anton on 15/10/15.
 */
public class Composition extends Music implements ILyric {

    private String text;

    public Composition(String name, Time time, String genre, String singer, String musician, String text){
        super(name, time, genre, singer, musician);
        this.text = text;
    }


    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + text.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj))
            return false;
        Composition other = (Composition)obj;
        if (!this.text.equals(other.getMusiciant()))
            return false;
        return true;
    }

}
