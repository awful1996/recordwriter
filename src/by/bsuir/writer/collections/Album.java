package by.bsuir.writer.collections;

import by.bsuir.writer.DemoLog;
import by.bsuir.writer.model.Record;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by anton on 22/10/15.
 */
public class Album {
    private static Logger logger = Logger.getLogger(DemoLog.class);
    private ArrayList<Record> arrayOfRecord = new ArrayList<>();

    @Override
    public ArrayList<Record> clone() throws CloneNotSupportedException {
        logger.info("create clone of object");
        ArrayList<Record> newArr = new ArrayList<>();
        for(Record item : arrayOfRecord){
            newArr.add(item);
        }
        return newArr;
    }

    public void setArrayOfRecord(ArrayList<Record> arrayOfRecord) {
        logger.info("set array of record");
        this.arrayOfRecord = arrayOfRecord;
    }

    public ArrayList<Record> getArrayOfRecord() {
        logger.info("get array of record");
        return arrayOfRecord;
    }
}
