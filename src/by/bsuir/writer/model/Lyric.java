package by.bsuir.writer.model;

/**
 * Created by anton on 15/10/15.
 */
public class Lyric extends Record implements ILyric {

    private String text;

    public Lyric(String name, Time time, String genre, String singer, String text){
        super(name, time, genre, singer);
        this.text = text;
    }

    public String getText(){
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        return super.hashCode()+text.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj))
            return false;
        Lyric other = (Lyric)obj;
        if (!this.text.equals(other.getText()))
            return false;
        return true;
    }

}
