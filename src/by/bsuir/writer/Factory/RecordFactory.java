package by.bsuir.writer.Factory;

import by.bsuir.writer.DemoLog;
import by.bsuir.writer.model.Composition;
import by.bsuir.writer.model.Lyric;
import by.bsuir.writer.model.Music;
import by.bsuir.writer.model.Record;
import by.bsuir.writer.model.Time;
import org.apache.log4j.Logger;


/**
 * Created by anton on 20/10/15.
 */
public class RecordFactory {

    private static Logger logger = Logger.getLogger(DemoLog.class);

    public static Record createMusic(String[] args){
        Time time = new Time();
        try{
            time.setHours(Integer.parseInt(args[1]));
            time.setMin(Integer.parseInt(args[2]));
            time.setSec(Integer.parseInt(args[3]));
        }
        catch(Exception e){
            logger.error("Object Time got incorrect data", e);
        }
        logger.info("Create Music ");
        return new Music(args[0],
                time,
                args[4],
                args[5],
                args[6]
        );
    }

    public static Record createLyric(String[] args){
        Time time = new Time();
        try{
            time.setHours(Integer.parseInt(args[1]));
            time.setMin(Integer.parseInt(args[2]));
            time.setSec(Integer.parseInt(args[3]));
        }
        catch(Exception e){
            logger.info("Object Time got incorrect data", e);
        }
        logger.info("Create lyric ");
        return new Lyric(args[0],
                time,
                args[4],
                args[5],
                args[6]
        );
    }

    public static Record createComposition(String[] args){
        Time time = new Time();
        try{
            time.setHours(Integer.parseInt(args[1]));
            time.setMin(Integer.parseInt(args[2]));
            time.setSec(Integer.parseInt(args[3]));
        }
        catch(Exception e){
            logger.error("Object Time got incorrect data", e);
        }
        logger.info("Create composition ");
        return new Composition(args[0],
                time,
                args[4],
                args[5],
                args[6],
                args[7]
        );
    }
}
