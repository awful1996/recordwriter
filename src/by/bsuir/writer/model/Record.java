package by.bsuir.writer.model;

/**
 * Created by anton on 17/10/15.
 */
public class Record {

    private String genre;
    private String name;
    private String singer;
    private Time time;

    public Record(String name, Time time, String genre, String singer) {
        this.name = name;
        this.time = time;
        this.genre = genre;
        this.singer = singer;
    }


    public String getGenre() {
        return genre;
    }


    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getName(){
        return name;
    }

    public Time getTime() {
        return time;
    }

    @Override
    public int hashCode() {
        return name.hashCode()+singer.hashCode()+time.hashCode()+genre.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Record other = (Record) obj;
        if (!this.name.equals(other.getName()))
            return false;
        if (!this.genre.equals(other.getGenre()))
            return false;
        if (!this.singer.equals(other.getSinger()))
            return false;
        if (!this.time.equals(other.getTime()))
            return false;
        return true;
    }
}
