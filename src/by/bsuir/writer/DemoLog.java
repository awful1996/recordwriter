package by.bsuir.writer;

import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by anton on 20/10/15.
 */
public class DemoLog {
    static {
        new DOMConfigurator().configure("log4j.xml");
    }
}
