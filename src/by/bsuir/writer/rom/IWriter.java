package by.bsuir.writer.rom;

import by.bsuir.writer.collections.Album;

/**
 * Created by anton on 16/10/15.
 */
public interface IWriter {
    void burn(Album album);
}
