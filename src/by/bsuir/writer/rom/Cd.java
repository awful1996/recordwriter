package by.bsuir.writer.rom;

import by.bsuir.writer.DemoLog;
import by.bsuir.writer.collections.Album;
import by.bsuir.writer.logic.ActionWithAlbum;
import by.bsuir.writer.model.Time;
import org.apache.log4j.Logger;

/**
 * Created by anton on 16/10/15.
 */
public class Cd implements IWriter {

    private Logger logger = Logger.getLogger(DemoLog.class);

    @Override
    public void burn(Album album) {
        try {
            if (ActionWithAlbum.getCommonTime(album.getArrayOfRecord()).compareTo(new Time(1, 20, 0))>0)
                logger.info("burn disk");
            else
                logger.error("can't burn disk, not enough space, try dvd");
        }
        catch(Exception e){
            logger.info("Object Time got incorrect data", e);
        }
    }
}
