package by.bsuir.writer.parser;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import by.bsuir.writer.DemoLog;
import by.bsuir.writer.Factory.RecordFactory;
import by.bsuir.writer.collections.Album;
import by.bsuir.writer.model.Record;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by anton on 18/10/15.
 */
public class Xml {

    private Logger logger = Logger.getLogger(DemoLog.class);

    public ArrayList<Record> openXmlFile(String nameOfFile){
        ArrayList<Record> album = new ArrayList<>();
        try {
            logger.info(" try open file");

            File fXmlFile = new File(nameOfFile);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("lyric");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    Record element = RecordFactory.createLyric(new String[]{eElement.getElementsByTagName("name").item(0).getTextContent(),
                            eElement.getElementsByTagName("hour").item(0).getTextContent(),
                                    eElement.getElementsByTagName("min").item(0).getTextContent(),
                                    eElement.getElementsByTagName("sec").item(0).getTextContent(),
                            eElement.getElementsByTagName("genre").item(0).getTextContent(),
                            eElement.getElementsByTagName("singer").item(0).getTextContent(),
                            eElement.getElementsByTagName("text").item(0).getTextContent()}
                    );
                    album.add(element);
                    logger.info("Add in album lyric");
                }
            }

            nList = doc.getElementsByTagName("music");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    Record element = RecordFactory.createMusic(new String[]{eElement.getElementsByTagName("name").item(0).getTextContent(),
                            eElement.getElementsByTagName("hour").item(0).getTextContent(),
                            eElement.getElementsByTagName("min").item(0).getTextContent(),
                            eElement.getElementsByTagName("sec").item(0).getTextContent(),
                            eElement.getElementsByTagName("genre").item(0).getTextContent(),
                            eElement.getElementsByTagName("singer").item(0).getTextContent(),
                            eElement.getElementsByTagName("musiciant").item(0).getTextContent()}
                    );
                    album.add(element);
                    logger.info("Add in album music ");
                }
            }

            nList = doc.getElementsByTagName("composition");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    Record element = RecordFactory.createComposition(new String[]{eElement.getElementsByTagName("name").item(0).getTextContent(),
                            eElement.getElementsByTagName("hour").item(0).getTextContent(),
                            eElement.getElementsByTagName("min").item(0).getTextContent(),
                            eElement.getElementsByTagName("sec").item(0).getTextContent(),
                            eElement.getElementsByTagName("genre").item(0).getTextContent(),
                            eElement.getElementsByTagName("singer").item(0).getTextContent(),
                            eElement.getElementsByTagName("musiciant").item(0).getTextContent(),
                            eElement.getElementsByTagName("text").item(0).getTextContent()}
                    );
                    album.add(element);
                    logger.info("Add in album composition ");
                }
            }

        } catch (Exception e) {
            logger.error("Error in file", e);
        }
        return album;
    }

}
