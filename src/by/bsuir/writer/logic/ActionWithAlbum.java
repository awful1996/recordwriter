package by.bsuir.writer.logic;

import by.bsuir.writer.DemoLog;
import by.bsuir.writer.Factory.RecordFactory;
import by.bsuir.writer.collections.Album;
import by.bsuir.writer.compare.CompareRecord;
import by.bsuir.writer.model.Record;
import by.bsuir.writer.model.Time;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by anton on 22/10/15.
 */
public class ActionWithAlbum {

    private static Logger logger = Logger.getLogger(DemoLog.class);


    public static ArrayList<Record> sortByGenre(ArrayList<Record> arr){
        logger.info("Sort record ");
        Collections.sort(arr, new CompareRecord());
        return arr;
    }

    public static ArrayList<Record> findRecord(Time from, Time to, ArrayList<Record> album){

        logger.info("Find record ");
        ArrayList<Record> array = new ArrayList<>();

        for(Record item : album){
            if((from.compareTo(item.getTime())>=0)&& to.compareTo(item.getTime())<=0 ){
                array.add(item);
            }
        }
        return array;
    }

    public static void printNameOfSong(ArrayList<Record> album) {
        logger.info("Print name of song ");
        for(Record item : album){
            System.out.println(item.getName());
        }
    }

    public static Time getCommonTime(ArrayList<Record> album){
        logger.info("Get common time ");
        int commonHour = 0,commonMinut = 0,commonSecond = 0;
        for(Record item : album){
            Time currentTime = item.getTime();
            commonHour += currentTime.getHours();
            commonMinut += currentTime.getMin();
            commonSecond += currentTime.getSec();
        }

        commonMinut += commonSecond/60;
        commonSecond %=60;
        commonHour +=commonMinut/60;
        commonMinut %= 60;
        Time time = new Time();
        time.setHours(commonHour);
        try{
            time.setMin(commonMinut);
            time.setSec(commonSecond);
        }
        catch(Exception e){
            logger.error("Object Time got incorrect data", e);
        }
        return time;
    }

}
