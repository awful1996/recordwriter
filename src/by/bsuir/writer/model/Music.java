package by.bsuir.writer.model;

/**
 * Created by anton on 15/10/15.
 */
public class Music extends Record {

    private String musiciant;

    public void setMusiciant(String musiciant) {
        this.musiciant = musiciant;
    }

    public String getMusiciant() {

        return musiciant;
    }

    public Music(String name, Time time, String genre, String singer, String musiciant) {
        super(name, time, genre, singer);
        this.musiciant = musiciant;
    }

    @Override
    public int hashCode() {
        return super.hashCode()+musiciant.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj))
            return false;
        Music other = (Music) obj;
        if (!this.musiciant.equals(other.getMusiciant()))
            return false;
        return true;
    }

}
